const cookies = [
  // allow essential cookies cookie -
  { name: "ig_did", value: "0975C5D5-9878-40B7-8E08-76B08BF6B641" },

  // unclear if required - but is assinged on allow essential cookies
  { name: "mid", value: "YmwmSQALAAH_sXZa0z2KqNEHFLYX" },
  // { name: "csrftoken", value: "QsOiAWZ9SwyMNcZOsnmrnrBUrgzdEHG3" },

  //logged in cookies
  {
    name: "rur",
    value: `"RVA\\05453080918723\\0541682791754:01f70637f448685e79db7c38f237a7b9704010b9062a720f0370c34f691515372e2c78da"`,
  },
  { name: "sessionid", value: "53080918723%3AM4VOWRxYNRkerC%3A13" },
  { name: "ds_user_id", value: "53080918723" },
];

module.exports = (session, domain, cookiesPath) => {
  session.defaultSession.clearStorageData([]);
  cookies.forEach((cookie) => {
    session.defaultSession.cookies.set({ url: domain, ...cookie }).then(
      () => console.info(`successfuly set ${cookie.name} = ${cookie.value}`),
      (err) => console.error(err)
    );
  });
};
