const { app, BrowserWindow, session } = require("electron");
const setupCookies = require("./cookiesManager");

const instushUrl = "https://www.instagram.com";

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    show: false,
    webPreferences: {
      webSecurity: false,
    },
  });

  win.loadURL(instushUrl);
  win.webContents.openDevTools();

  win.once("ready-to-show", () => {
    win.show();
  });
};

app.whenReady().then(() => {
  setupCookies(session, instushUrl, "");
  createWindow();
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});
